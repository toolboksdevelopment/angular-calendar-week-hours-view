import { InjectionToken, Inject } from '@angular/core';
import { CalendarDateFormatter, DateFormatterParams } from 'angular-calendar';
import { DatePipe } from '@angular/common';

import moment from 'moment/src/moment';

export class CustomDateFormatter extends CalendarDateFormatter {

    public weekViewShortWeekDay({ date, locale }: DateFormatterParams): string {
        return moment(date)
          .locale(locale)
          .format('dd');
      }

      public weekViewOnlyDay({ date, locale }: DateFormatterParams): string {
        return moment(date)
          .locale(locale)
          .format('DD');
      }

      public weekViewShortMonth({ date, locale }: DateFormatterParams): string {
        return moment(date)
          .locale(locale)
          .format('MMM');
      }

}
